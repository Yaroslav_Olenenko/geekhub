package com.yaroslav.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by Yaroslav on 22.01.2015.
 */

public class FileManager {
    private final String mainDir;
    private String pathToFile = ""; // path to files with mainDir
    //  storage folder
    public FileManager(String mainDir) {
        this.mainDir = mainDir;
    }

    public Map<String, String> getFileList(String path) {
        Map<String, String> fileList = new HashMap<>(Collections.EMPTY_MAP);
        File dir;
        //  Moment when we are opening not main dir
        if (path != null) {
            String fullPath = mainDir + "\\" + path; // get full path
            dir = new File(fullPath);
            if (dir.isDirectory()) {
                for (File file : dir.listFiles()) {
                    fileList.put(file.getName(),
                            file.getPath().substring(file.getPath().indexOf("\\upload\\") + 8, file.getPath().length()));
                }
            } else {
                // read file and return content to show in web page
                try {
                    String content = Files.readAllLines(dir.toPath()).stream().collect(Collectors.joining("\n"));
                    fileList.put("file:" + dir.getPath().substring(dir.getPath().indexOf("\\upload\\") + 8,
                            dir.getPath().length()), content);
                } catch (IOException e) {
                    fileList.put("file:", "Problem with read file " + dir.getName() + "</br>" + e.getMessage());
                }
            }
        } else {
            dir = new File(mainDir);
            if (dir.isDirectory()) {
                for (File file : dir.listFiles()) {
                    String key = file.getName();
                    String value = file.getPath().substring(file.getPath().indexOf("\\upload\\") + 8,
                            file.getPath().length());
                    fileList.put(key, value);
                }
            }
        }
        return fileList;
    }

    public boolean deleteFile(String filePath) {
        filePath = mainDir + "\\" + filePath;
        File file = new File(filePath);
        if (file.isFile()) {
            if (file.exists()) {
                System.out.println(filePath);
                return file.delete();
            } else return false;
        } else return removeDirectory(file);
    }

    private boolean removeDirectory(File file) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null && files.length > 0) {
                for (File vFile : files) {
                    removeDirectory(vFile);
                }
            }
            return file.delete();
        } else {
            return file.delete();
        }
    }

    public void updateFile(String filePath, String content) {
        String fullPath = mainDir + "\\" + filePath;
        File file = new File(fullPath);
        if (file.exists()) {
            if (file.isFile()) {
                file.delete();
            }
            try {
                FileWriter fw = new FileWriter(file);
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(content);
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean createDirectory(String dirPath, String name) {
        boolean success;
        Random random = new Random();
        if (name != null) {
            success = (new File(dirPath + "\\" + name)).mkdir();
        } else {
            success = (new File(dirPath + "\\" + Integer.toString(random.nextInt(1000)))).mkdir();
        }
        return success;
    }
}
