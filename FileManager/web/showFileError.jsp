<%--
  Created by IntelliJ IDEA.
  User: Yaroslav
  Date: 22.01.2015
  Time: 15:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title></title>
  <link rel="stylesheet" type="text/css" href="style/css/css.css">

  <script>
    function goBack(){
      window.history.back();
    }
  </script>
</head>
<body>
<p align="center" style="padding-top: 15%; color:white;font-weight: bold;font-size: 20px">
  Problem with opening file. </br>
  It`s can be happened because of File format not Text.</br>
  <a onclick="goBack()" style="color:red">return back</a>
</p>
</body>
</html>
