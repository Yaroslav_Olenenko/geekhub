package com.yaroslav.model;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * Created by Yaroslav on 15.02.2015.
 */

@Entity
@Table(name = "group")
public class Group {

    @Id
    @Column(name = "id_group")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idGroup;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "group")
    private Set<User> users;

    public int getIdGroup() {
        return idGroup;
    }

    public void setIdGroup(int idGroup) {
        this.idGroup = idGroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
}
