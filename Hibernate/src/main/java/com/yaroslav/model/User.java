package com.yaroslav.model;

import javax.persistence.*;

/**
 * Created by Yaroslav on 15.02.2015.
 */

@Entity
@Table(name = "user")
public class User {

    @Id
    @Column(name = "id_user")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idUser;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "email")
    private String email;

    @ManyToOne
    @JoinColumn(name = "fk_group")
    private Group group;

    public User() {}

    public User(String userName, String email, Group group) {
        setUserName(userName);
        setEmail(email);
        setGroup(group);
    }
    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
