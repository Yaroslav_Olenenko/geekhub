package com.yaroslav.service;

import com.yaroslav.model.Group;
import com.yaroslav.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;

/**
 * Created by Yaroslav on 16.02.2015.
 */

@Repository
@Transactional
public class GroupService {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurentSession() {
        return sessionFactory.getCurrentSession();
    }

    public void save(Group group) {
        getCurentSession().saveOrUpdate(group);
    }

    public Group findByIdUnique(int id) {
        Group group = (Group) getCurentSession().get(Group.class, id);
        if (group == null) return null;
        group.setUsers(new HashSet<User>(findMembers(group)));
        return group;
    }

    public List<User> findMembers(Group group) {
        return getCurentSession().createQuery("FROM User u where u.group.idGroup = :g_id")
                .setParameter("g_id", group.getIdGroup()).list();
    }

    public List<Group> getGroups() {
        List<Group> groups = getCurentSession().createCriteria(Group.class).list();
        for(Group group : groups) group.setUsers(new HashSet<User>(findMembers(group)));
        return groups;
    }

    public void delete(Group group) {
        if (group != null) {
            if (findByIdUnique(group.getIdGroup())  != null) {
                getCurentSession().delete(group);
            }
        }
    }
}
