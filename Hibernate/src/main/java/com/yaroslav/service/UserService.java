package com.yaroslav.service;

import com.yaroslav.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yaroslav on 16.02.2015.
 */

@Repository
@Transactional
public class UserService {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurentSession() {
        return sessionFactory.getCurrentSession();
    }

    public void saveUser(User user) {
        getCurentSession().saveOrUpdate(user);
    }

    public User getUser(Integer id) {
        return (User) getCurentSession().get(User.class, id);
    }

    public List<User> getUsers() {

        return new ArrayList<User>(getCurentSession().createCriteria(User.class).list());
    }

    public void deleteUser(User user) {
        getCurentSession().delete(user);
    }
}
