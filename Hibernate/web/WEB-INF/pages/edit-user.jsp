<%--
  Created by IntelliJ IDEA.
  User: Yaroslav
  Date: 16.02.2015
  Time: 12:02
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title></title>
</head>
<body>

<form:form modelAttribute="user" commandName="user" action="/user/update" method="post">
  <select  hidden="hidden" name="userId"><option value="${user.idUser}" selected="true"/></select>
  First Name:<form:input path="userName"/>
  Email:<form:input  path="email"/>
  Group:
  <select name="groupId">
    <c:forEach var="group" items="${groups}">
      <c:choose>
        <c:when test="${group.id eq user.group.id}">
          <option value="${group.id}" selected="true">${group.name}</option>
        </c:when>
        <c:otherwise>
          <option value="${group.id}">${group.name}</option>
        </c:otherwise>
      </c:choose>
    </c:forEach>
  </select>
  <button type="submit">UPDATE</button>
</form:form>
</body>
</html>