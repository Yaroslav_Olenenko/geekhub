<%--
  Created by IntelliJ IDEA.
  User: Yaroslav
  Date: 16.02.2015
  Time: 12:03
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<body>
<h1>Hibernate Example</h1>
<form action="${pageContext.request.contextPath}/users" method="post">
  <input name="firstName" placeholder="First Name">
  <input name="lastName" placeholder="Last Name">
  <input name="email" type="email" placeholder="Email">
  <select name="groupId">
    <c:forEach var="group" items="${groups}">
      <option value="${group.id}">${group.name}</option>
    </c:forEach>
  </select>
  <input type="submit" value="Submit">
</form>

<a href="/groups">Groups</a>
</br>
<table border="1">
  <tr>
    <td>ID</td>
    <td>FIRST NAME</td>
    <td>LAST NAME</td>
    <td>EMAIL</td>
    <td>GROUP</td>
  </tr>
  <c:forEach var="user" items="${users}">
    <tr>
      <td>${user.id}</td>
      <td>${user.firstName}</td>
      <td>${user.lastName}</td>
      <td>${user.email}</td>
      <td>
        <p>

          <form:form method="POST" id="leave" action="/user${user.id}/leaveGroup">
            ${user.group.name}
            <c:if test="${not empty user.group}">
              <a onclick="document.forms['leave'].submit()">(<span style="color:red">leave</span>)</a>
            </c:if>
          </form:form>

        </p>
      </td>
      <td>
        <a style="color:green" href="/user${user.id}/edit">edit</a>
        <form id="removeUser" action="/user${user.id}/delete" method="post">
          <a style="color:red" onclick="document.forms['removeUser'].submit()">delete</a>
        </form>
      </td>
    </tr>
  </c:forEach>
</table>
</body>
</html>
