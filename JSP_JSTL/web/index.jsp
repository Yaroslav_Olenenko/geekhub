<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/functions" %>
<%--
  Created by IntelliJ IDEA.
  User: Yaroslav
  Date: 22.01.2015
  Time: 14:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title></title>
  <link rel="stylesheet" type="text/css" href="style/css/css.css">
</head>
<body>

<table align="center">
  <tr>
    <td>File name</td>
    <td align="center">Value</td>
    <td align="center">Action</td>
  </tr>
  <tbody>
    <c:forEach var="item" items="${sessionScope}">
      <tr>
        <form action="/delete" method="get">
          <td>
             ${item.key}
          </td>
          <td>
            ${item.value}
          </td>
          <td>
            <a href="?name=${item.key}&action=delete">delete</a>
          </td>
        </form>
      </tr>
    </c:forEach>
    <tr>
      <form action="/add" method="get" >
        <td>
          <input type="text" name="name">
        </td>
        <td>
          <input type="text" name="value">
        </td>
        <td>
          <input type="submit" name="action" value="add">
        </td>
      </form>
    </tr>
  </tbody>
</table>
</body>
</html>
