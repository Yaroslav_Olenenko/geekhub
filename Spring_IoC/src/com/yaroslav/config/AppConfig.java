package com.yaroslav.config;

import com.yaroslav.language.LanguageDetector;
import com.yaroslav.main.Dictionary;
import com.yaroslav.main.TextSource;
import com.yaroslav.main.Translator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * Created by Yaroslav on 08.02.2015.
 */

@Configuration
public class AppConfig {

    @Bean(name = "textSource")
    public TextSource textSource() {
        return new TextSource();
    }

    @Bean(name = "languageDetector")
    public LanguageDetector languageDetector() {
        return new LanguageDetector();
    }

    @Bean(name = "dictionary")
    public Dictionary dictionary() {
        return new Dictionary();
    }

    @Bean(name = "translator")
    public Translator translator() {
        return new Translator();
    }

}
