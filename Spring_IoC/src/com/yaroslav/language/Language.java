package com.yaroslav.language;

/**
 * Created by Yaroslav on 08.02.2015.
 */
public enum Language {
    ENGLISH, RUSSIAN;
}
