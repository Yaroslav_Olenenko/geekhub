package com.yaroslav.language;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Yaroslav on 08.02.2015.
 */
public class LanguageDetector {
    private final Pattern ENGLISH = Pattern.compile("[a-zA-Z]+");
    private final Pattern RUSSIAN = Pattern.compile("[а-яА-ЯёЁъЪ]+");

    private final String delimiter = "[^a-zA-Zа-яА-ЯёЁъЪ]+";
    private Map<Language, Pattern> langPatterns;

    private String text;
    public Language detectLanguage(String text) {
        Map<Language, Integer> counter = new HashMap<>();
        this.text = text;
        
        if (langPatterns == null) {
            initPatterns();
        }
        
        langPatterns.entrySet()
                .stream()
                .forEach(entry -> counter.put(entry.getKey(), getCountOfMatches(entry.getValue())));

        Language translateLang = counter.entrySet()
                .stream()
                .max(Map.Entry.comparingByValue(Integer::compareTo))
                .get().getKey();

        return translateLang;
    }

    private Integer getCountOfMatches(Pattern pattern) {
        int countOfMatches = 0;

        for (String word : text.split(delimiter)) {
            Matcher matcher = pattern.matcher(word);
            while (matcher.find()) {
                countOfMatches++;
            }
        }

        return countOfMatches;
    }

    private void initPatterns() {
        langPatterns = new HashMap<Language, Pattern>() {{
            put(Language.ENGLISH, ENGLISH);
            put(Language.RUSSIAN, RUSSIAN);
        }};
    }
}
