package com.yaroslav.main;

import com.yaroslav.config.AppConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by Yaroslav on 08.02.2015.
 */
public class Controller {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

        Translator translator = context.getBean(Translator.class);
        String translation = translator.translate("d:/1.txt");
        System.out.println(translation);
    }
}
