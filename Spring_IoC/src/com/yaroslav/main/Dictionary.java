package com.yaroslav.main;

import com.yaroslav.language.Language;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Yaroslav on 08.02.2015.
 */
public class Dictionary {
    private Map<Language, Map<String, String>> dictionaries = new HashMap<Language, Map<String, String>>();

    //don't translate first word
    public String translate(String word, Language language) {
        Map<String, String> dictionary = getDictionary(language);
        String translation = dictionary.get(word);
        return translation == null ? word :  translation;
    }

    private Map<String, String> getDictionary(Language language) {
        Map<String, String> dictionary = dictionaries.get(language);
        if (dictionary == null) {
            dictionary = loadDictionary(language);
            dictionaries.put(language, dictionary);
        }
        return dictionary;
    }

    private Map<String, String> loadDictionary(Language language) {
        Map<String, String> dict = new HashMap<String, String>();
        List<String> lines = ResourceLoader.getInstance().load("Spring_IoC/dict/" + language.name().toLowerCase() + ".dict");
        for (String line : lines) {
            String[] parts = line.split("=");
            dict.put(parts[0], parts[1]);
        }
        return dict;
    }
}
