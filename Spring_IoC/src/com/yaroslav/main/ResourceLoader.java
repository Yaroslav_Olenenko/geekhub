package com.yaroslav.main;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yaroslav on 08.02.2015.
 */
public class ResourceLoader {
    private static ResourceLoader instance = new ResourceLoader();

    private ResourceLoader() {}

    public static ResourceLoader getInstance() {
        return instance;
    }

    public List<String> load(String source) {
        try {
            return Files.readAllLines(Paths.get(source), Charset.defaultCharset());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<String>();
    }
}
