package com.yaroslav.main;

import java.util.List;

/**
 * Created by Yaroslav on 08.02.2015.
 */
public class TextSource {

    public String getText(String path) {
        List<String> load = ResourceLoader.getInstance().load(path);
        StringBuilder sb = new StringBuilder();
        for (String s : load) {
            sb.append(s);
        }
        return sb.toString();
    }
}
