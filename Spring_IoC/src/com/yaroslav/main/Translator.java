package com.yaroslav.main;

import com.yaroslav.language.Language;
import com.yaroslav.language.LanguageDetector;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Yaroslav on 08.02.2015.
 */
public class Translator {

    private Set<Language> languages;

    @Autowired
    private TextSource textSource;

    @Autowired
    private Dictionary dictionary;

    @Autowired
    private LanguageDetector languageDetector;

    public Translator() {

    }

    public String translate(String source) {
        String text = textSource.getText(source);
        Language language = languageDetector.detectLanguage(text);
        String[] words = text.split(" ");
        StringBuilder sb = new StringBuilder();
        for (String word : words) {
            String translatedWord = dictionary.translate(word, language);
            sb.append(translatedWord + " ");
        }
        return sb.toString();
    }

    public Set<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<Language> languages) {
        this.languages = languages;
    }

    public LanguageDetector getLanguageDetector() {
        return languageDetector;
    }

    public void setLanguageDetector(LanguageDetector languageDetector) {
        this.languageDetector = languageDetector;
    }

    public Dictionary getDictionary() {
        return dictionary;
    }

    public void setDictionary(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    public TextSource getTextSource() {
        return textSource;
    }

    public void setTextSource(TextSource textSource) {
        this.textSource = textSource;
    }
}
