package lesson1.Task1;

import java.util.Scanner;

public class Factorial {
    public int fact(int n){
        if (n == 1) {
            return 1;
        }
        else {
            return n*fact(n-1);
        }
    }

    public static void main(String []args){
        System.out.print("Enter n -> ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        Factorial f = new Factorial();
        System.out.println("Result = " + f.fact(n));
    }
}
