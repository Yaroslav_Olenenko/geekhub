package lesson1.Task2;

import java.util.Scanner;

public class Fibonacci {

    int phi (int n) {
        if (n==0||n==1){
            return 1;
        } else {
            return phi(n-1) + phi(n-2);
        }
    }

    public static void main(String[] args){
        System.out.print("Enter n -> ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        Fibonacci f = new Fibonacci();

        for (int i=0; i<=n; i++){
            System.out.println(i + " -> " + f.phi(i));
        }
    }
}
