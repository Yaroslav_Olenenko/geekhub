package lesson1.Task3;

import java.util.Scanner;

/**
 * Created by Home on 18.10.2014.
 */
public class Number {

    public static String num[] = {"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight"};

    public String outputSwith(int n) {
        switch (n) {
            case 1:
               return "Один";
            case 2:
               return "Два";
            case 3:
               return "Три";
            case 4:
               return "Чотири";
            case 5:
               return "Пять";
            case 6:
               return "Шість";
            case 7:
               return "Сім";
            case 8:
               return "Вісім";
            case 9:
               return "Девять";
            default:
               return "Введіть цифру";
        }
    }

    public String outputIf(int n){
        if (n == 0) {
            return "Zero";
        } else if (n == 1) {
            return "One";
        } else if (n == 2) {
            return "Two";
        } else if (n == 3) {
            return "Three";
        } else if (n == 4) {
            return "Four";
        } else if (n == 5) {
            return "Five";
        } else if (n == 6) {
            return "Six";
        } else if (n == 7) {
            return "Seven";
        } else if (n == 8) {
            return "Eight";
        } else if (n == 9) {
            return "Nine";
        }
        return "Error!";
    }

    public String outputArray(int n){
        if (n>=0 && n<10) {
            return num[n];
        }
        return "Error!";
    }

    public static void main(String []args) {
        System.out.print("Enter n (n>=0 and n<10) -> ");
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        Number number = new Number();
        System.out.println("First method swith " + number.outputSwith(n));
        System.out.println("Second method if " + number.outputIf(n));
        System.out.println("Third method array " + number.outputArray(n));
    }
}
