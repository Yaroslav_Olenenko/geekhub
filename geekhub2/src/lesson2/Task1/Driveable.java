package lesson2.Task1;

/**
 * Created by Home on 25.10.2014.
 */
public interface Driveable {
    public void accelerate();

    public void brake();

    public void turn(int direct);

    public void start();

    public void stop();

    public void refuel();
}
