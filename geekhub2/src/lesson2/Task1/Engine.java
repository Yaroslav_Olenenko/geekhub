package lesson2.Task1;

/**
 * Created by Home on 25.10.2014.
 */
public class Engine {
    private boolean work = false;
    private double speed = 0;
    private final double MAX_SPEED = 140;

    public boolean isWork() {
        return work;
    }

    public void setWork(boolean work) {
        this.work = work;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        if (speed <= MAX_SPEED) {
            this.speed = speed;
        }
    }
}
