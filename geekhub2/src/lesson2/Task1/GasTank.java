package lesson2.Task1;

/**
 * Created by Home on 25.10.2014.
 */
public class GasTank {
    private double volume = 0;
    private final double MAX_VOLUME = 100;
    private final double GAS_KILOMETER = 0.65;

    GasTank(int volume) {
        this.setVolume(volume);
    }

    public double getVolume() {
        return volume;
    }

    public boolean setVolume(double volume) {
        if (volume + this.volume <= this.MAX_VOLUME) {
            this.volume += volume;
        } else {
            System.out.print("Max volume Gas-Tank = 100. Please enter correct value!!!!");
            return false;
        }
        return true;
    }

    public boolean toEngine(double volume) {
        this.volume -= volume * GAS_KILOMETER;
        if (this.volume < 0) {
            this.volume = 0;
        }
        return this.volume > 0;
    }

    public void reFuel() {
        volume = MAX_VOLUME;
    }
}
