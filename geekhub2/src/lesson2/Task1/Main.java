package lesson2.Task1;

import java.util.Scanner;

/**
 * Created by Home on 25.10.2014.
 */
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int input;
        int it = 0;
        boolean exit = false;
        Vehicle vehicle = new Vehicle();
        while (true) {
            it++;
            System.out.println("Iteration = " + it);
            System.out.println("Enter 1 for on engine");
            System.out.println("Enter 2 for off engine");
            System.out.println("Enter 3 for turn left");
            System.out.println("Enter 4 for turn right");
            System.out.println("Enter 5 for accelerate");
            System.out.println("Enter 6 for brake");
            System.out.println("Enter 7 for refuel");
            System.out.println("Enter 0 exit");
            input = in.nextInt();
            System.out.println("!---------------------!");
            switch (input) {
                case 1:
                    vehicle.start();
                    break;
                case 2:
                    vehicle.stop();
                    break;
                case 3:
                    vehicle.turn(1);
                    break;
                case 4:
                    vehicle.turn(0);
                    break;
                case 5:
                    vehicle.accelerate();
                    break;
                case 6:
                    vehicle.brake();
                    break;
                case 7:
                    vehicle.refuel();
                    break;
                case 0:
                    exit = true;
                    break;
                default:
                    System.out.println("Please enter number 0..7");
            }
            System.out.println("!---------------------!");
            if (exit) {
                break;
            }
        }
    }
}
