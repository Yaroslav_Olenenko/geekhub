package lesson2.Task1;

/**
 * Created by Home on 25.10.2014.
 */
public class Vehicle implements Driveable {
    private Engine engine = new Engine();
    private Wheels wheels = new Wheels(1000);
    private GasTank gasTank = new GasTank(100);

    @Override
    public void accelerate() {
        if (engine.isWork()) {
            engine.setSpeed(engine.getSpeed() + 10);
            System.out.println("Speed vehicle = " + engine.getSpeed());
        } else {
            System.out.println("Engine don't work");
        }
    }

    @Override
    public void brake() {
        if (engine.isWork()) {
            engine.setSpeed(engine.getSpeed() - 10);
            System.out.println("Speed vehicle = " + engine.getSpeed());
        } else {
            System.out.println("Engine don't work");
        }
    }

    @Override
    public void turn(int direct) {
        if (engine.getSpeed() <= 40
                && engine.getSpeed() >= 10) {
            if (engine.isWork()) {
                switch (direct) {
                    case 0:
                        System.out.println("Vehicle turn Right");
                        gasTank.toEngine(engine.getSpeed());
                        wheels.turn();
                        System.out.println("Fuel = " + gasTank.getVolume());
                        if (gasTank.getVolume() == 0) {
                            engine.setWork(false);
                            System.out.println("Fuel ended. Engine don't work!!!");
                        }
                        break;
                    case 1:
                        System.out.println("Vehicle turn Left");
                        gasTank.toEngine(engine.getSpeed());
                        System.out.println("Fuel = " + gasTank.getVolume());
                        wheels.turn();
                        if (gasTank.getVolume() == 0) {
                            engine.setWork(false);
                            System.out.println("Fuel ended. Engine don't work!!!");
                        }
                        break;
                }
            } else {
                System.out.println("Engine don't work");
            }
        } else {
            System.out.println("For to return you should reduce speed to 40 km and speed should be not less than 10 km");
        }
    }

    @Override
    public void start() {
        if ((gasTank.getVolume() > 0) && (wheels.getDeteriorationAfter() > 0)) {
            engine.setWork(true);
            System.out.println("Burrr....");
        } else {
            if (gasTank.getVolume() == 0) {
                System.out.println("The tank is empty");
            }
            if (wheels.getDeteriorationAfter() == 0) {
                System.out.println("wheels of the car is damaged, contact a service center.");
            }
        }
    }

    @Override
    public void stop() {
        engine.setWork(false);
        System.out.println("Puh puh...");
    }

    @Override
    public void refuel() {
        gasTank.reFuel();
        System.out.println("Gas Ok");
    }
}
