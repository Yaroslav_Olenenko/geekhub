package lesson2.Task2;

/**
 * Created by Home on 27.10.2014.
 */
public class GasTank {
    private double volume = 0;
    private final double VOLUME_MAX = 150;
    private final double GAS_KILOMETER = 0.15;

    GasTank(int volume) {
        if (volume > 100)
            this.volume = 100;
        else
            this.volume = volume;
    }

    public double getVolume() {
        return volume;
    }

    public boolean setVolume(double volume) {
        if (volume + this.volume <= this.VOLUME_MAX) {
            this.volume += volume;
        } else {
            System.out.print("Max volume Gas-Tank = 150. Please enter correct value!!!!");
            return false;
        }
        return true;
    }

    public boolean toEngine(double volume) {
        this.volume -= volume * GAS_KILOMETER;
        if (this.volume < 0) {
            this.volume = 0;
        }
        return this.volume > 0;
    }

    public void reFuel() {
        volume = VOLUME_MAX;
    }
}
