package lesson2.Task2;

/**
 * Created by Home on 27.10.2014.
 */
public class Screw {
    //rotation
    private boolean rotation = false;
    private int timeWork = 1000;

    public boolean isWork() {
        return timeWork > 0;
    }
    public int getTimeWork() {
        return timeWork;
    }

    public void setTimeWork(int timeWork) {
        this.timeWork = timeWork;
    }

    public boolean isRotation() {
        return rotation;
    }

    public void turn() {
        this.timeWork -=1;
    }

    public void setRotation(boolean rotation) {
        this.rotation = rotation;
    }
}
