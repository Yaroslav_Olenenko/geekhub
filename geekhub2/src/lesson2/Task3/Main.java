package lesson2.Task3;

import lesson2.Task1.Driveable;
import lesson2.Task1.Vehicle;
import lesson2.Task3.SunVehicle;

import java.util.Scanner;

/**
 * Created by Home on 27.10.2014.
 */
public class Main {
    public static void chooseCar(Driveable vehicle) {
        Scanner in = new Scanner(System.in);
        int input;
        int it = 0;
        boolean exit = false;
        while (true) {
            it++;
            System.out.println("Iteration = " + it);
            System.out.println("Enter 1 for start engine");
            System.out.println("Enter 2 for stop engine");
            System.out.println("Enter 3 for turn left");
            System.out.println("Enter 4 for turn right");
            System.out.println("Enter 5 for accelerate");
            System.out.println("Enter 6 for brake");
            System.out.println("Enter 7 for refuel");
            System.out.println("Enter 0 exit");
            input = in.nextInt();
            System.out.println("!---------------------!");
            switch (input) {
                case 1:
                    vehicle.start();
                    break;
                case 2:
                    vehicle.stop();
                    break;
                case 3:
                    vehicle.turn(1);
                    break;
                case 4:
                    vehicle.turn(0);
                    break;
                case 5:
                    vehicle.accelerate();
                    break;
                case 6:
                    vehicle.brake();
                    break;
                case 7:
                    vehicle.refuel();
                    break;
                case 0:
                    exit = true;
                    break;
                default:
                    System.out.println("Please enter number 0..7");
            }
            System.out.println("!---------------------!");
            if (exit) {
                break;
            }
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int input;
        boolean exit = true;
        do {
            System.out.println("choose vehicle 0, 1 and other for exit");
            System.out.println("0 vehicle with gas engine");
            System.out.println("1 vehicle with solar battery");
            input = in.nextInt();
            switch (input) {
                case 0:
                    Vehicle vehicle = new Vehicle();
                    chooseCar(vehicle);
                    break;
                case 1:
                    SunVehicle sunVehicle = new SunVehicle();
                    chooseCar(sunVehicle);
                    break;
                default:
                    exit = false;
            }
        }
        while (exit);
    }
}
