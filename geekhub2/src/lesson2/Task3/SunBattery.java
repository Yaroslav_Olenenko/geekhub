package lesson2.Task3;

/**
 * Created by Home on 27.10.2014.
 */
public class SunBattery {
    private boolean sunny = false;
    private int hour = 0;

    public int getHour() {
        return this.hour;
    }

    public boolean isSunny() {
        if ((this.hour >= 6) && (this.hour <= 19)) {
            this.sunny = true;
        } else {
            this.sunny = false;
        }
        return this.sunny;
    }

    public void setSunny(boolean sunny) {
        this.sunny = sunny;
    }

    public void oneIteration() {
        this.hour++;
        this.hour = (this.hour < 24)
                ? this.hour
                : 0;
    }

    public void Sleep() {
        this.hour = 6;
    }
}
