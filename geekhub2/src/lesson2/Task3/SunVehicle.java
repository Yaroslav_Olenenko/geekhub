package lesson2.Task3;
/**
 * Created by Home on 27.10.2014.
 */

import lesson2.Task1.Driveable;

public class SunVehicle implements Driveable {
    private ElectroEngine electroEngine = new ElectroEngine();
    private Wheels wheels = new Wheels(1000);
    private SunBattery sunBattery = new SunBattery();

    @Override
    public void accelerate() {
        sunBattery.oneIteration();
        if (!sunBattery.isSunny()) {
            electroEngine.setWork(sunBattery.isSunny());
            System.out.println("Now the night. solar panel does not work!!!");
        }
        if (electroEngine.isWork()) {
            electroEngine.setSpeed(electroEngine.getSpeed() + 10);
            System.out.println("Speed vehicle = " + electroEngine.getSpeed());
        } else {
            System.out.println("Engine don't work");
        }
    }

    @Override
    public void brake() {
        sunBattery.oneIteration();
        if (!sunBattery.isSunny()) {
            electroEngine.setWork(sunBattery.isSunny());
            System.out.println("Now the night. solar panel does not work!!!");
        }
        if (electroEngine.isWork()) {
            electroEngine.setSpeed(electroEngine.getSpeed() - 10);
            System.out.println("Speed vehicle = " + electroEngine.getSpeed());
        } else {
            System.out.println("Engine don't work");
        }
    }

    @Override
    public void turn(int direct) {
        if ((electroEngine.getSpeed() <= 40) && (electroEngine.getSpeed() >= 10)) {
            if (electroEngine.isWork()) {
                switch (direct) {
                    case 0:
                        System.out.println("Vehicle turn Right");
                        sunBattery.oneIteration();
                        if (!sunBattery.isSunny()) {
                            electroEngine.setWork(sunBattery.isSunny());
                            System.out.println("Now the night. solar panel does not work!!!");
                            break;
                        }
                        wheels.turn();
                        System.out.println("Hour = " + sunBattery.getHour());
                        break;
                    case 1:
                        System.out.println("Vehicle turn Left");
                        sunBattery.oneIteration();
                        if (!sunBattery.isSunny()) {
                            electroEngine.setWork(sunBattery.isSunny());
                            System.out.println("Now the night. solar panel does not work!!!");
                            break;
                        }
                        wheels.turn();
                        System.out.println("Hour = " + sunBattery.getHour());
                        break;
                }
            } else {
                System.out.println("Engine don't work");
            }
        } else {
            System.out.println("For to return you should reduce speed to 40 km and speed should be not less than 10 km");
        }
    }

    @Override
    public void start() {
        sunBattery.oneIteration();
        if (!electroEngine.isWork()) {
            if (sunBattery.isSunny() && (wheels.getDeteriorationAfter() > 0)) {
                electroEngine.setWork(true);
                System.out.println("HHHHHHHHH....");
            } else {
                if (!sunBattery.isSunny()) {
                    System.out.println("Now the night. solar panel does not work!!!");
                }
                if (wheels.getDeteriorationAfter() == 0) {
                    System.out.println("wheels of the car is damaged, contact a service center.");
                }
            }
        } else {
            System.out.println("Vehicle working now");
        }
    }

    @Override
    public void stop() {
        sunBattery.oneIteration();
        if (electroEngine.isWork()) {
            electroEngine.setWork(false);
            System.out.println("HHHHHHhhhhhhhh..........");
        } else {
            System.out.println("Vehicle don't working now");
        }
    }

    @Override
    public void refuel() {
        sunBattery.Sleep();
        System.out.println("Battery Ok");
    }
}
