package lesson2.Task3;

/**
 * Created by Home on 27.10.2014.
 */
public class Wheels {
    public int deteriorationAfter = 1000;

    Wheels (int deteriorationAfter) {
        this.deteriorationAfter = deteriorationAfter;
    }

    public int getDeteriorationAfter() {
        return deteriorationAfter;
    }

    public void setDeteriorationAfter(int deteriorationAfter) {
        this.deteriorationAfter = deteriorationAfter;
    }

    public boolean OneKilometer() {
        this.deteriorationAfter -= 1;
        if (this.deteriorationAfter < 0) {
            this.deteriorationAfter = 0;
        }
        return this.deteriorationAfter != 0;
    }

    public boolean turn(){
        this.deteriorationAfter -= 4;
        if (this.deteriorationAfter < 0) {
            this.deteriorationAfter = 0;
        }
        return this.deteriorationAfter != 0;
    }
}
