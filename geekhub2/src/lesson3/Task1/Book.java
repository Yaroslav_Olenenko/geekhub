package lesson3.Task1;

/**
 * Created by Home on 09.11.2014.
 */
public class Book implements Comparable {
    public int year;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public int compareTo(Object o) {
        Book anotherBook = (Book) o;

        if (anotherBook.year != this.year) {
            if (this.year < anotherBook.year) {
                return -1;
            } else {
                return 1;
            }
        }
        return 0;
    }
}
