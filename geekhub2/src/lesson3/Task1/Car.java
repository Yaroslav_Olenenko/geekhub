package lesson3.Task1;

/**
 * Created by Home on 09.11.2014.
 */
public class Car implements Comparable {
    public int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public int compareTo(Object o) {
        Car anotherCar = (Car) o;

        if (anotherCar.age != this.age) {
            if (this.age < anotherCar.age) {
                return -1;
            } else {
                return 1;
            }
        }
        return 0;
    }


}
