package lesson3.Task1;

import java.util.Scanner;

/**
 * Created by Home on 09.11.2014.
 */
public class Main {

    public static Comparable[] sort(Comparable[] elements) {
        int i;
        int j;
        boolean flag = true;
        Comparable tmp;
        Comparable el[] = new Comparable[elements.length];
        for (i = 0; i < el.length; i++) {
            el[i] = elements[i];
        }
        while (flag) {
            flag = false;
            for (j = 0; j < el.length - 1; j++) {
                if (el[j].compareTo(el[j + 1]) == -1) {
                    tmp = el[j];
                    el[j] = el[j + 1];
                    el[j + 1] = tmp;
                    flag = true;
                }
            }
        }
        return el;
    }

    public static void main(String[] args) {

        boolean cycle = true;
        int n;
        int i;

        Scanner in = new Scanner(System.in);
        while (cycle) {
            System.out.println("Choose Book or Car (0 or 1)");
            System.out.println("Enter 9 for exit");

            n = in.nextInt();
            switch (n) {
                case 0:
                    System.out.println("Enter count Books");

                    n = in.nextInt();
                    Book books[] = new Book[n];

                    for (i = 0; i < books.length; i++) {
                        books[i] = new Book();
                        books[i].setYear(-(int) (Math.random() * 150) + 2014);
                        //System.out.println(car[i].age);
                    }
                    Comparable[] books2 = sort(books);

                    for (i = 0; i < books.length; i++) {
                        System.out.println(books[i].getYear() + " -> " + ((Book) books2[i]).getYear());
                    }
                    break;
                case 1:
                    System.out.println("Enter count Cars");
                    n = in.nextInt();
                    Car car[] = new Car[n];

                    for (i = 0; i < car.length; i++) {
                        car[i] = new Car();
                        car[i].setAge(-(int) (Math.random() * 50) + 2014);
                    }

                    Comparable[] car2 = sort(car);

                    for (i = 0; i < car.length; i++) {
                        System.out.println(car[i].getAge() + " -> " + ((Car) car2[i]).getAge());
                    }
                    break;
                case 9:
                    cycle = false;
                    break;
                default:
                    System.out.println("You must enter number 0, 1 or 9");
            }
        }

    }
}
