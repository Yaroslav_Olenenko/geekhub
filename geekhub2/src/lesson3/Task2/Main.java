package lesson3.Task2;

/**
 * Created by Home on 10.11.2014.
 */
public class Main {

    public static long testString() {
        long cntBefore = System.currentTimeMillis();
        int i;
        String str = "str";
        for (i = 0; i < 100000; i++) {
            str += "1";
        }
        long cntAfter = System.currentTimeMillis();
        return cntAfter - cntBefore;
    }

    public static long testStringBuilder() {
        long cntBefore = System.currentTimeMillis();
        int i;
        StringBuilder str = new StringBuilder("str");
        for (i = 0; i < 100000; i++) {
            str.append("1");
        }
        long cntAfter = System.currentTimeMillis();
        return cntAfter - cntBefore;
    }

    public static long testStringBuffer() {
        long cntBefore = System.currentTimeMillis();
        int i;
        StringBuffer str = new StringBuffer("str");
        for (i = 0; i < 100000; i++) {
            str.append("1");
        }
        long cntAfter = System.currentTimeMillis();
        return cntAfter - cntBefore;
    }

    public static void main(String[] args) {
        System.out.println("Speed Test concatenating strings");
        System.out.println("String count milliseconds = " + testString());
        System.out.println("StringBuilder cont milliseconds = " + testStringBuilder());
        System.out.println("StringBuffer cont milliseconds = " + testStringBuffer());
    }
}
