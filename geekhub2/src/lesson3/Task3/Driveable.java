package lesson3.Task3;

/**
 * Created by Home on 25.10.2014.
 */
public interface Driveable {
    public void accelerate();

    public void brake();

    public void drive100Km();

    public void turn(int direct);

    public void start();

    public void stop();

    public void reFuel();

    public void addFuel10L();

    public void remainderFuel();
}
