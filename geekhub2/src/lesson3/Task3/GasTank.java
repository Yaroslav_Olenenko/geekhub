package lesson3.Task3;

/**
 * Created by Home on 25.10.2014.
 */
public class GasTank {
    private double volume = 0;
    private final double MAX_VOLUME = 100;
    private final double GAS_KILOMETER = 0.65;

    public GasTank() {
        try {
            this.setVolume(100);
        } catch (OverflowTankException e) {
            e.printStackTrace();
        }
    }

    public double getVolume() {
        return volume;
    }

    public boolean setVolume(double volume) throws OverflowTankException{
        if (volume + this.volume <= this.MAX_VOLUME) {
            this.volume += volume;
        } else {
            throw new OverflowTankException();
            //return false;
        }
        return true;
    }

    public boolean toEngine(double volume) throws RunOutFuelException {
        this.volume -= volume * GAS_KILOMETER;
        if (this.volume < 0) {
            this.volume = 0;
            throw new RunOutFuelException();
        }
        return this.volume > 0;
    }

    public void reFuel() {
        volume = MAX_VOLUME;
    }
}
