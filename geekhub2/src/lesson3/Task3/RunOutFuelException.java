package lesson3.Task3;

/**
 * Created by Home on 12.11.2014.
 */

// run out of fuel
public class RunOutFuelException extends Exception {

    public RunOutFuelException() {
        System.out.println("Run out of fuel");
    }
}
