package lesson3.Task3;

/**
 * Created by Home on 25.10.2014.
 */
public class Vehicle implements Driveable {
    private Engine engine = new Engine();
    private Wheels wheels = new Wheels(1000);
    private GasTank gasTank = new GasTank();

    @Override
    public void accelerate() {
        if (engine.isWork()) {
            try {
                engine.setSpeed(engine.getSpeed() + 10);
            } catch (ExcessSpeedException e) {
                e.printStackTrace();
            }
            System.out.println("Speed vehicle = " + engine.getSpeed());
        } else {
            System.out.println("Engine don't work");
        }
    }

    @Override
    public void brake() {
        if (engine.isWork()) {
            try {
                engine.setSpeed(engine.getSpeed() - 10);
            } catch (ExcessSpeedException e) {
                e.printStackTrace();
            }
            System.out.println("Speed vehicle = " + engine.getSpeed());
        } else {
            System.out.println("Engine don't work");
        }
    }

    @Override
    public void turn(int direct) {
        if (engine.getSpeed() <= 40
                && engine.getSpeed() >= 10) {
            if (engine.isWork()) {
                switch (direct) {
                    case 0:
                        System.out.println("Vehicle turn Right");
                        try {
                            gasTank.toEngine(engine.getSpeed());
                        } catch (RunOutFuelException e) {
                            e.printStackTrace();
                        }
                        try {
                            wheels.turn();
                        } catch (BrokenWheelException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Fuel = " + gasTank.getVolume());
                        if (gasTank.getVolume() == 0) {
                            engine.setWork(false);
                            System.out.println("Fuel ended. Engine don't work!!!");
                        }
                        break;
                    case 1:
                        System.out.println("Vehicle turn Left");
                        try {
                            gasTank.toEngine(engine.getSpeed());
                        } catch (RunOutFuelException e) {
                            e.printStackTrace();
                        }
                        System.out.println("Fuel = " + gasTank.getVolume());
                        try {
                            wheels.turn();
                        } catch (BrokenWheelException e) {
                            e.printStackTrace();
                        }
                        if (gasTank.getVolume() == 0) {
                            engine.setWork(false);
                            System.out.println("Fuel ended. Engine don't work!!!");
                        }
                        break;
                }
            } else {
                System.out.println("Engine don't work");
            }
        } else {
            System.out.println("For to return you should reduce speed to 40 km and speed should be not less than 10 km");
        }
    }

    @Override
    public void start() {
        if ((gasTank.getVolume() > 0) && (wheels.getDeteriorationAfter() > 0)) {
            engine.setWork(true);
            System.out.println("Burrr....");
        } else {
            if (gasTank.getVolume() == 0) {
                System.out.println("The tank is empty");
            }
            if (wheels.getDeteriorationAfter() == 0) {
                System.out.println("wheels of the car is damaged, contact a service center.");
            }
        }
    }

    @Override
    public void stop() {
        engine.setWork(false);
        System.out.println("Puh puh...");
    }

    @Override
    public void reFuel() {
        gasTank.reFuel();
        System.out.println("Gas Ok");
    }

    @Override
    public void drive100Km() {
        if (engine.getSpeed() > 0) {
            try {
                gasTank.toEngine(100);
            } catch (RunOutFuelException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void addFuel10L()  {
        try {
            gasTank.setVolume(10);
        } catch (OverflowTankException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void remainderFuel() {
        System.out.println("Remaind Fuel = " + gasTank.getVolume());
    }
}
