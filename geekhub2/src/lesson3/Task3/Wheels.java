package lesson3.Task3;

/**
 * Created by Home on 25.10.2014.
 */
public class Wheels {
    public int deteriorationAfter = 1000;

    Wheels (int deteriorationAfter) {
        this.deteriorationAfter = deteriorationAfter;
    }

    public int getDeteriorationAfter() {
        return deteriorationAfter;
    }

    public void setDeteriorationAfter(int deteriorationAfter) {
        this.deteriorationAfter = deteriorationAfter;
    }

    public boolean OneKilometer() throws BrokenWheelException {
        this.deteriorationAfter -= 1;
        if (this.deteriorationAfter < 0) {
            this.deteriorationAfter = 0;
            throw new BrokenWheelException();
        }
        return this.deteriorationAfter != 0;
    }

    public boolean turn() throws BrokenWheelException {
        this.deteriorationAfter -= 4;
        if (this.deteriorationAfter < 0) {
            this.deteriorationAfter = 0;
            throw new BrokenWheelException();
        }
        return this.deteriorationAfter != 0;
    }
}
