package lesson4.Task1;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by Home on 14.11.2014.
 */
public class Main {
    public static void main(String[] args) {
        Set<Integer> setA = new HashSet<Integer>();
        Set<Integer> setB = new HashSet<Integer>();
        MySet operations = new MySet();
        int i;
        for (i = 0; i < 10; i++) {
            setA.add((int) (Math.random() * 50));
            setB.add((int) (Math.random() * 50));
        }

        System.out.println("Set A ");
        System.out.print(setA);
        System.out.println();
        System.out.println("Set B");
        System.out.print(setB);
        System.out.println();

        System.out.println("Select one of the operations on sets.");
        System.out.println("0 equals");
        System.out.println("1 Union");
        System.out.println("2 Intersect");
        System.out.println("3 Sets");
        System.out.println("4 Subtract Sets");
        Scanner in = new Scanner(System.in);
        i = in.nextInt();
        switch (i) {
            case 0:
                if (operations.equals(setA, setB)) {
                    System.out.println("Sets equals");
                } else {
                    System.out.println("Sets not equals");
                }
                break;
            case 1:
                System.out.println("Union Sets");
                System.out.print(operations.union(setA, setB));
                break;
            case 2:
                System.out.println("Intersect Sets");
                System.out.print(operations.intersect(setA, setB));
                break;
            case 3:
                System.out.println("Substract Sets");
                System.out.print(operations.subtract(setA, setB));
                break;
            case 4:
                System.out.println("Symmetric Subtract Sets");
                System.out.print(operations.symmetricSubtract(setA, setB));
                break;
            default:
                System.out.println("Please enter number 0..4");
        }
    }
}
