package lesson4.Task1;

import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Home on 14.11.2014.
 */
public class MySet implements SetOperations{

    @Override
    public  <T> boolean equals(Set<T> a, Set<T> b) {
        return a.equals(b);
    }

    @Override
    public <T> Set<T> union(Set<T> a, Set<T> b) {
        Set<T> tmp = new TreeSet<T>(a);
        tmp.addAll(b);
        return tmp;
    }

    @Override
    public <T> Set<T> subtract(Set<T> a, Set<T> b) {
        Set<T> tmp = new TreeSet<T>(a);
        tmp.removeAll(b);
        return tmp;
    }

    @Override
    public <T> Set<T> intersect(Set<T> a, Set<T> b) {
        Set<T> tmp = new TreeSet<T>();
        for (T x : a)
            if (b.contains(x)) {
                tmp.add(x);
            }
        return tmp;
    }

    @Override
    public <T> Set<T> symmetricSubtract(Set<T> a, Set<T> b) {
        Set<T> tmpA;
        Set<T> tmpB;

        tmpA = union(a, b);
        tmpB = intersect(a, b);
        return subtract(tmpA,tmpB);
    }
}
