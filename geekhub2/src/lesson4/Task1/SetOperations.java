package lesson4.Task1;

import java.util.Set;

/**
 * Created by Home on 14.11.2014.
 */

public interface SetOperations {

    public <T> boolean equals(Set<T> a, Set<T> b);

    public <T> Set<T> union(Set<T> a, Set<T> b);

    public <T> Set<T> subtract(Set<T> a, Set<T> b);

    public <T> Set<T> intersect(Set<T> a, Set<T> b);

    public <T> Set<T> symmetricSubtract(Set<T> a, Set<T> b);
}

