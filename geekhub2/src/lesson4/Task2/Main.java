package lesson4.Task2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

/**
 * Created by Home on 18.11.2014.
 */
public class Main {
    public static void message(String arg) {
        System.out.println(arg);
    }

    public static Date createDate(int dd, int mm, int yy) {
        Calendar c = new GregorianCalendar();
        c.set(Calendar.HOUR_OF_DAY, 0); //anything 0 - 23
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.DAY_OF_MONTH, dd);
        c.set(Calendar.MONTH, mm);
        c.set(Calendar.YEAR, yy);
        Date date = c.getTime();
        return date;
    }

    public Date createCurrentDate() {
        Calendar c = new GregorianCalendar();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        Date date = c.getTime();
        return date;
    }

    public static void main(String[] args) throws IOException {
        message("Hi! Welcome to this task manager.");
        TaskManager taskManager = new TaskManagerImpl();
        Date date;
        int dd;
        int mm;
        int yy;
        String category;
        String name;
        String description;
        Task task;
        boolean b = false;
        Scanner in = new Scanner(System.in);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n;
        while (true) {
            message("Enter 0 to exit");
            message("Enter 1 to add task");
            message("Enter 2 to remove task");
            message("Enter 3 to display all task");
            n = in.nextInt();
            switch (n) {
                case 0:
                    b = true;
                    break;
                case 1:
                    message("Create task");
                    message("Please enter Date dd, mm, yyyy");
                    dd = in.nextInt();
                    mm = in.nextInt();
                    yy = in.nextInt();
                    message("Please enter Category task");
                    category = br.readLine();
                    message("Please enter name task");
                    name = br.readLine();
                    message("Please enter description task");
                    description = br.readLine();
                    task = new Task(category, name, description);
                    taskManager.addTask(createDate(dd, mm, yy), task);
                    break;
                case 2:
                    message("Delete task");
                    message("Please enter Date dd, mm, yyyy");
                    dd = in.nextInt();
                    mm = in.nextInt();
                    yy = in.nextInt();
                    taskManager.removeTask(createDate(dd, mm, yy));
                    break;
                case 3:
                    message("Display all task");
                    message(taskManager.toString());
                    break;
                default:
                    message("Error");
            }
            if (b) {
                break;
            }
        }
    }
}
