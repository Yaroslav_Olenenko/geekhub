package lesson4.Task2;

/**
 * Created by Home on 15.11.2014.
 */
public class Task {
    private String category;
    private String description;
    private String name;

    public Task (String category, String description, String name) {
        this.category = category;
        this.description = description;
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return "------Task-------\n" +
                "-category = " + category + "-\n" +
                "-name = " + name + "-\n" +
                "-desctiption = " + description + "-\n" +
                "----------------";
    }
}
