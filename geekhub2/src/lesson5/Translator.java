package lesson5;

import lesson5.source.URLSourceProvider;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Provides utilities for translating texts to russian language.<br/>
 * Uses Yandex Translate API, more information at <a href="http://api.yandex.ru/translate/">http://api.yandex.ru/translate/</a><br/>
 * Depends on {@link URLSourceProvider} for accessing Yandex Translator API service
 */
public class Translator {
    private URLSourceProvider urlSourceProvider;
    /**
     * Yandex Translate API key could be obtained at <a href="http://api.yandex.ru/key/form.xml?service=trnsl">http://api.yandex.ru/key/form.xml?service=trnsl</a>
     * to do that you have to be authorized.
     */
    private static final String YANDEX_API_KEY = "trnsl.1.1.20141122T141233Z.93c23c585ed13613.a30575baf318d231a5a80bb62828e31a5534a2e9";
    private static final String TRANSLATION_DIRECTION = "ru";

    public Translator(URLSourceProvider urlSourceProvider) {
        this.urlSourceProvider = urlSourceProvider;
    }

    /**
     * Translates text to russian language
     * @param original text to translate
     * @return translated text
     * @throws IOException
     */
    public String translate(String original) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();

        if (original.contains(".")) {
            for (String line : original.split("[.]")) {
                stringBuilder.append(parseContent(urlSourceProvider.load(prepareURL(line)))).append(".");
            }
        } else {
            stringBuilder.append(parseContent(urlSourceProvider.load(prepareURL(original))));
        }
        return stringBuilder.toString();
    }

    /**
     * Prepares URL to invoke Yandex Translate API service for specified text
     * @param text to translate
     * @return url for translation specified text
     */
    private String prepareURL(String text) {
        return "https://translate.yandex.net/api/v1.5/tr/translate?key=" +
                YANDEX_API_KEY + "&text=" + encodeText(text) + "&lang=" +
                TRANSLATION_DIRECTION;
    }

    /**
     * Parses content returned by Yandex Translate API service. Removes all tags and system texts. Keeps only translated text.
     * @param content that was received from Yandex Translate API by invoking prepared URL
     * @return translated text
     */
    private String parseContent(String content) {
        if (content.equals("CONNECTION FAIL")) {
            return "[ lost text ]";
        }
        String text = content.substring(content.indexOf("<text>") + 6, content.indexOf("</text>"));
        //text = text.replace("**", "\n");
        return text;
    }

    /**
     * Encodes text that need to be translated to put it as URL parameter
     * @param text to be translated
     * @return encoded text
     */
    private String encodeText(String text) {
        try {
            return URLEncoder.encode(text, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "error";
    }
}
