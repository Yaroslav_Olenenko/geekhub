package lesson6.adapters;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Converts object of type java.util.Date to String by using dd/MM/yyyy format
 */
public class DateAdapter implements JsonDataAdapter<Date> {
    @Override
    public Object toJson(Date date) {
        DateFormat format = new SimpleDateFormat();
        String formatedDate = format.format(date);
        return "(" + formatedDate + ")";
    }
}
