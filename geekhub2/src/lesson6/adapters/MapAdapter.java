package lesson6.adapters;

import lesson6.json.JsonSerializer;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Converts all objects that extends java.util.Map to JSONObject.
 */

public class MapAdapter implements JsonDataAdapter<Map> {
    @Override
    public Object toJson(Map map) throws JSONException {
        JSONObject m = new JSONObject();
        map.keySet().stream().forEach(key -> {
            try {
                m.put(String.valueOf(key), JsonSerializer.serialize(map.get(key)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
        return m;
    }
}
