package lesson7;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Utils class that contains useful method to interact with URLConnection
 */
public class ConnectionUtils {
    private static final  int BUFFER_SIZE_IN_KB = 2 * 1024;
    /**
     * Downloads content for specified URL and returns it as a byte array.
     * Should be used for small files only. Don't use it to download big files it's dangerous.
     * @param url
     * @return
     * @throws java.io.IOException
     */
    public static byte[] getData(URL url) throws IOException {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        byte[] buffer = new byte[BUFFER_SIZE_IN_KB];

        URLConnection connection = url.openConnection();
        int len;
        try (BufferedInputStream reader = new BufferedInputStream(connection.getInputStream())){
            while ( ( len = reader.read(buffer) ) > 0) {
                bytes.write(buffer, 0, len);
            }
        }

        return bytes.toByteArray();
    }
}
