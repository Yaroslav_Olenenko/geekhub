package lesson8.storage;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class QueryBuilder {
    public String generateUpdateQuery(Map<String, Object> data, String tableName) {
        StringBuilder queryBuilder = new StringBuilder();
        String splitSymbol = "::";
        queryBuilder.append("UPDATE ").append(tableName).append(" SET ");
        String keys = data.keySet().stream()
                .filter(key -> !"id".equals(key))
                .collect(Collectors.joining(splitSymbol));

        String values = data.values().stream()
                .filter(value -> null != value)
                .map(Object::toString)
                .collect(Collectors.joining(splitSymbol));

        for (int i = 0; i < keys.split(splitSymbol).length; i++) {
            String key = keys.split(splitSymbol)[i];
            String value = "'" + values.split(splitSymbol)[i] + "'";
            queryBuilder.append(key).append("=").append(value).append(value).append(",");
        }
        queryBuilder.delete(queryBuilder.lastIndexOf(","), queryBuilder.length());
        queryBuilder.append(" WHERE id = '").append(data.get("id")).append("'");
        return queryBuilder.toString();
    }

    public String generateInsertQuery(Map<String, Object> data, String tableName) {
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("INSERT INTO ").append(tableName).append("(");

        String keys = data.keySet().stream()
                .filter(key -> null != data.get(key))
                .collect(Collectors.joining(","));
        queryBuilder.append(keys).append(")").append(" VALUES ('");

        String values = data.values().stream()
                .filter(value -> null != value)
                .map(Object::toString).collect(Collectors.joining("','"));
        queryBuilder.append(values).append("')");

        return queryBuilder.toString();
    }

    public int getInsertKey(ResultSet resultSet) throws SQLException {
        int id = 0;
        if (resultSet.next()) {
            id = resultSet.getInt(1);
        }
        return id;
    }
}
